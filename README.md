# Net-Ethereum version 0.30.1

The README is used to introduce the module and provide instructions on how to install the module, any machine dependencies it may have (for example C compilers and installed libraries) and any other information that should be provided before the module is installed.

A README file is required for CPAN modules since CPAN extracts the README file from a module distribution so that people browsing the archive can use it get an idea of the modules uses. It is usually a good idea to provide version information here so that people can decide whether fixes for the module are worth downloading.

#### INSTALLATION

To install this module type the following:

```bash
perl Makefile.PL
make
make test
make install
```

#### DEPENDENCIES

This module requires these other modules and libraries:

```perl
use HTTP::Request;
use LWP::UserAgent;
use JSON;
use Math::BigInt;
use Math::BigFloat;
use File::Slurper 'read_text';
use Data::Dumper;
```

#### COPYRIGHT AND LICENCE

Copyright (C) 2018 by Alexandre Frolov

This library is free software; you can redistribute it and/or modify it (under the same terms as Perl itself)[https://www.gnu.org/licenses/gpl-3.0.ru.html], either Perl version 5.26.0 or, at your option, any later version of Perl 5 you may have available.

#### LINKS AND CREDITS

[CPAN](http://search.cpan.org/~itmfrolov/Net-Ethereum-0.30/lib/Net/Ethereum.pm)

[METACPAN](https://metacpan.org/pod/Net::Ethereum)

[Habrahabr](https://habr.com/post/346234/)

[Author](https://www.facebook.com/frolov.shop2you)

[Contrubutor](https://gitlab.com/pheix)
